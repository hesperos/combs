import pytest

from combs import combs

def test_make_result_parser():
    value = 123
    inp = "some input string"
    p = combs.make_result_parser(value)
    res = p(inp)

    assert not combs.is_error(inp)

    rv, rinp = res[0]
    assert rv == value
    assert rinp == inp


def test_optional_whitespace_parsing_on_string_with_no_whitespaces():
    wp = combs.make_optional_parser(
        combs.make_many_times_string_parser(
            combs.make_whitechar_parser()))

    no_ws_input = "there is no whitespaces to parse here"
    res = wp(no_ws_input)

    assert not combs.is_error(res)

    rv, rinp = res[0]

    assert rv == ""
    assert rinp == no_ws_input


def test_optional_whitespace_parsing_on_string_with_whitespaces():
    wp = combs.make_optional_parser(
        combs.make_many_times_string_parser(
            combs.make_whitechar_parser()))

    ws_input = "   there are some whitespaces here"
    expected = "   "
    res = wp(ws_input)
    assert not combs.is_error(res)

    rv, rinp = res[0]
    assert rv == expected
    assert rinp == ws_input[len(expected):]


def test_if_whitespace_rejection_is_possible():
    ws_p = combs.make_optional_parser(
        combs.make_many_times_string_parser(
            combs.make_whitechar_parser()))

    w_p = combs.make_many_times_string_parser(
        combs.make_alphanum_parser())

    p = combs.make_single_result_parser(
        combs.make_take_right_parser(ws_p, w_p))

    ws_input = "   there are some whitespaces here"
    no_ws_input = "there is no whitespaces to parse here"
    expected = "there"
    ws = "   "

    res1 = p(ws_input)
    assert not combs.is_error(res1)

    rv1, rinp1 = res1[0]
    assert rv1 == expected
    assert rinp1 == ws_input[len(ws) + len(expected):]

    res2 = p(no_ws_input)
    assert not combs.is_error(res2)

    rv2, rinp2 = res2[0]
    assert rv2 == expected
    assert rinp2 == no_ws_input[len(expected):]

