def make_result_parser(value):
    def parser(inp: str):
        return [(value, inp)]
    return parser


def make_zero_parser():
    def parser(inp: str):
        return []
    return parser


def is_error(res) -> bool:
    return len(res) == 0


def bind_parser(pa, binder):
    """
    parser(a) -> (a -> parser(b)) -> parser(b)
    """
    def parser(inp: str):
        results = []
        for res in pa(inp):
            rv, rinp = res
            pb = binder(rv)
            results.extend(pb(rinp))
        return results
    return parser


def map_parser(pa, mapper):
    """
    (parser(a)) -> (a -> b) -> parser(b)
    """
    def parser(inp: str):
        results = []
        for res in pa(inp):
            rva, rinpa = res
            rvb = mapper(rva)
            results.append((rvb, rinpa))

    return parser


def make_item_parser():
    def parser(inp: str):
        if len(inp) == 0:
            return make_zero_parser()(inp)
        return make_result_parser(inp[0])(inp[1:])
    return parser


def make_sat_parser(pred):
    return bind_parser(make_item_parser(), lambda c:
                       make_result_parser(c) if pred(c) else make_zero_parser())


def make_anyof_parser(pa, pb):
    def parser(inp: str):
        return pa(inp) + pb(inp)
    return parser


def make_exact_char_parser(c):
    return make_sat_parser(lambda r: c == r)


def make_digit_parser():
    return make_sat_parser(str.isdigit)


def make_lower_parser():
    return make_sat_parser(str.islower)


def make_upper_parser():
    return make_sat_parser(str.isupper)


def make_space_parser():
    return make_sat_parser(str.isspace)


def make_tabs_parser():
    return make_sat_parser(lambda c: c == '\t')


def make_newline_parser():
    return make_sat_parser(lambda c: c == '\n' or c == '\r')


def make_whitechar_parser():
    return make_anyof_parser(
        make_anyof_parser(make_space_parser(), make_tabs_parser()),
        make_newline_parser())


def make_alpha_parser():
    return make_anyof_parser(
        make_lower_parser(),
        make_upper_parser())


def make_alphanum_parser():
    return make_anyof_parser(
        make_alpha_parser(),
        make_digit_parser())


def make_exact_times_parser(pa, n):
    def parser(inp: str):
        if n == 0:
            return make_zero_parser()(inp)

        elif n == 1:
            return pa(inp)

        else:
            return bind_parser(pa, lambda x:
                               bind_parser(make_exact_times_parser(pa, n - 1), lambda y:
                                           make_result_parser(str(x) + y)))(inp)

    return parser


def make_many_times_parser(pa, reducer, initial_value):
    """
    apply pa until parse-error encountered
    """
    p1 = bind_parser(pa, lambda x:
                     bind_parser(make_many_times_parser(pa, reducer, initial_value), lambda y:
                                 make_result_parser(reducer(x, y))))

    p2 = make_result_parser(initial_value)

    return make_anyof_parser(p1, p2)


def make_many_times_string_parser(pa):
    return make_many_times_parser(pa, lambda a, b: str(a) + b, "")


def make_word_parser():
    return make_many_times_string_parser(
        make_alphanum_parser())


def make_take_right_parser(pa, pb):
    def parser(inp: str):
        resa = pa(inp)

        results = []
        for res in resa:
            rv, rinp = res
            resb = pb(rinp)
            results.extend(resb)
        return results

    return parser


def make_take_left_parser(pa, pb):
    def parser(inp: str):
        resa = pa(inp)
        results = []
        for res in resa:
            rva, rinpa = res
            for resb in pb(rinpa):
                rvb, rinpb = resb
                results.append((rva, rinpb))
        return results

    return parser


def make_prefix_parser(prefix: str):
    def parser(inp: str):
        if len(prefix) == 0:
            return make_result_parser("")(inp)

        return bind_parser(make_exact_char_parser(prefix[0]), lambda x:
                           bind_parser(make_prefix_parser(prefix[1:]), lambda y:
                                       make_result_parser(str(x) + y)))(inp)

    return parser


def make_optional_parser(pa):
    def parser(inp: str):
        res = pa(inp)
        if is_error(res):
            return make_result_parser(None)(inp)
        return res
    return parser


def make_single_result_parser(pa):
    def parser(inp: str):
        res = pa(inp)
        if is_error(res):
            return res
        return res[:1]

    return parser
