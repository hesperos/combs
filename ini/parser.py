from combs import combs

import argparse
from pprint import pprint

class Section(object):
    def __init__(self, name, key_values):
        self.name = name
        self.key_values = key_values

    def __repr__(self):
        return f"Section: {self.name}, entries: {self.key_values}"

    def __str__(self):
        return self.__repr__()


class Ini(object):
    def __init__(self, sections):
        self.sections = sections

    def __repr__(self):
        return f"Ini: {self.sections}"

    def __str__(self):
        return self.__repr__()


def make_whitechar_parser():
    return combs.make_optional_parser(
        combs.make_single_result_parser(
            combs.make_many_times_string_parser(
                combs.make_whitechar_parser())))


def make_word_parser():
    return combs.make_single_result_parser(
        combs.make_word_parser())


def make_section_name_parser():
    white_p = make_whitechar_parser()
    word_p = make_word_parser()

    sname_p = combs.bind_parser(
        combs.make_exact_char_parser('['), lambda left:
        combs.bind_parser(word_p, lambda name:
            combs.bind_parser(
                combs.make_exact_char_parser(']'), lambda right:
                combs.make_result_parser(name))))

    return combs.make_take_right_parser(white_p, sname_p)


def make_key_value_parser(delimiter = '='):
    word_p = make_word_parser()
    white_p = make_whitechar_parser()
    delim_p = combs.make_exact_char_parser(delimiter)

    str_p = combs.make_take_right_parser(white_p, word_p)
    delim_p = combs.make_take_right_parser(white_p, delim_p)

    kvp = combs.bind_parser(str_p, lambda key:
                            combs.bind_parser(delim_p, lambda delim:
                                              combs.bind_parser(str_p, lambda val:
                                                                combs.make_result_parser((key, val)))))
    return kvp


def make_entries_parser(delimiter = '='):
    reducer = lambda a, b: [a] + b
    initial_value = []
    return combs.make_single_result_parser(
        combs.make_many_times_parser(
            make_key_value_parser(delimiter),
            reducer,
            initial_value))


def make_section_parser():
    sname_p = make_section_name_parser()
    section_p = combs.bind_parser(
        sname_p, lambda section_name:
        combs.bind_parser(make_entries_parser(), lambda entries:
                          combs.make_result_parser(Section(section_name, entries))))
    return section_p


def make_sections_parser():
    initial_value = []
    reducer = lambda a, b: [a] + b
    section_p = make_section_parser()

    sections_p = combs.make_many_times_parser(section_p, reducer, initial_value)

    sections_p = combs.make_single_result_parser(sections_p)

    return combs.bind_parser(sections_p, lambda s:
                             combs.make_result_parser(Ini(s)))

def make_ini_parser():
    return make_sections_parser()


def make_argparse():
    ap = argparse.ArgumentParser(prog = "inip",
                                 description = "ini file parser written using parser combinators")
    ap.add_argument("file",
                    type = argparse.FileType('r'))
    return ap


def main():
    ini_parser = make_ini_parser()
    ap = make_argparse()
    args = ap.parse_args()
    contents = args.file.read()
    ini_file = ini_parser(contents)
    pprint(ini_file)
