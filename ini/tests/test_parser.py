import pytest

from ini import parser
from combs import combs

def test_if_parser_works_with_an_example_file_contents():
    ini_doc = """
[section1]
other = value2
key =  value
last = val

[section2]
some = more
keys = with
different  = values
  with = whitespaces
"""
    p = parser.make_ini_parser()
    ini_file = p(ini_doc)

    assert not combs.is_error(ini_file)
    rv, _ = ini_file[0]

    assert type(rv) == parser.Ini
    assert len(rv.sections) == 2
    assert len(rv.sections[0].key_values) == 3
    assert len(rv.sections[1].key_values) == 4
    assert rv.sections[0].name == "section1"
    assert rv.sections[1].name == "section2"

    assert rv.sections[0].key_values[0][0] == "other"
    assert rv.sections[0].key_values[0][1] == "value2"
    assert rv.sections[0].key_values[1][0] == "key"
    assert rv.sections[0].key_values[1][1] == "value"
    assert rv.sections[0].key_values[2][0] == "last"
    assert rv.sections[0].key_values[2][1] == "val"

    assert rv.sections[1].key_values[0][0] == "some"
    assert rv.sections[1].key_values[0][1] == "more"
    assert rv.sections[1].key_values[1][0] == "keys"
    assert rv.sections[1].key_values[1][1] == "with"
    assert rv.sections[1].key_values[2][0] == "different"
    assert rv.sections[1].key_values[2][1] == "values"
    assert rv.sections[1].key_values[3][0] == "with"
    assert rv.sections[1].key_values[3][1] == "whitespaces"
