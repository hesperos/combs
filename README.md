# Parser combinators in Python

This project is as example implementation of a concept of parser combinators in
Python as originally described in [monadic parser combinators](https://www.cs.nott.ac.uk/~pszgmh/monparsing.pdf) paper.

## Building

[poetry](https://python-poetry.org/) is required to build the project.

### Building a package

    poetry build

### Running tests

    poetry run tox

... or:

    poetry run pytest

### Running locally

    poetry install
    poetry run inip ./ini/tests/data/example.ini

## Details

This project is part of a [blog post](https://twdev.blog/2022/09/parser_combinators/) on [twdev.blog](https://twdev.blog).

You might be also interested in a Python prototype, implementing the same
concept which can be found [here](https://gitlab.com/hesperos/combspp).
